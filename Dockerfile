FROM docker:latest
RUN apk add --no-cache \
        libstdc++ \
        && apk add --no-cache --virtual .build-deps \
            binutils-gold \
            curl \
            g++ \
            gcc \
            gnupg \
            libgcc \
            linux-headers \
            make \
            python \
            autoconf \
            libpng-dev
RUN apk add --no-cache --update nodejs nodejs-npm
RUN npm i -g process-nextick-args util-deprecate